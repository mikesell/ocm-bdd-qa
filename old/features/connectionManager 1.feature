Feature: Connection Manager connection via ID or DID

# [IDM.OCM.E1.00006-11] are subject of this feature-file
  **CM**
    *Connection Manager*.
  **BCE**
    Block Connection Endpoint.
  **SSIAS**
    SSI Abstraction Service.

  Background: Interfaces are running
    Given that CM is running
      And that SSIAS is running

  Scenario: Block Connection by ID
  #[IDM.OCM.E1.00006]
    Given input connection ID `xx1` provided
    When a request to CM is made
    Then get http 200:Success code
      And the connection is deleted from SSIAS
      And the connection is marked as blocked in CM

#  Scenario: Block Connection by DID
#  #[IDM.OCM.E1.00006]
#    Given the request format is valid
#    When a request with a DID is made
#    Then the DID is accepted
#      And the connection is deleted from SSIAS
#      And the connection is marked as blocked in CM
#
#  Scenario Outline: Connection with invalid or missing ID/DID
#  # [IDM.OCM.E1.00006]
#  Given the request format is valid with ID: <ID> and DID: <DID>
#  When a request is attempted
#  Then the connection is not accepted
#    And the connection is not deleted from SSIAS
#    And the connection is not marked as blocked in CM
#
#  Examples:
#    | ID      | DID     |
#    | not set | not set |
#    | not set | invalid |
#    | invalid | not set |
#
#  Scenario Outline: Connection with a blocked connection is refused
#   # in this case by using connection id, ID or DID is meant
#   # [IDM.OCM.E1.00007]
#    Given format of the request is valid using <ID Blocked YN> and <DID Blocked YN>
#    When a request is attempted
#    Then connection is not accepted
#      And connection is not deleted from SSIAS
#      And connection is not marked as blocked in CM
#      And a response with the expected <refusal response> is returned
#
#     #  The values in this Examples table must be filled with exact values
#     Examples:
#     | ID Blocked YN     | DID Blocked YN    | refusal response |
#     | valid but blocked | valid but blocked | 404 Error        |
#     | valid but blocked | valid not blocked | Bad Request      |
#     | valid not blocked | valid but blocked | Bad Request      |
#
#    Scenario Outline: Auto-accept Connections to Self and Trusted connection to self
#   # [IDM.OCM.E1.00008 + 9]
#      Given endpoint is provided
#        And DID is DID of OCM
#      When connection is attempted at <endpoint> with <DID>
#      Then the connection is accepted
#        And the connection is marked as trusted in CM
#
#      Examples:
#      | endpoint | DID |
#      | to be defined | OCM DID |
#      | to be defined | not a OCM DID |
#
#    Scenario: Accepted Connection Automatically Using TSA
#    # [IDM.OCM.E1.000010]
#      Given endpoint is provided
#        And DID is of OCM
#      When connection is attempted with <DID> using TSA
#      Then the connection is accepted
#        And the connection is marked as trusted in CM
#
#    Scenario: Blocked Connection Automatically Using TSA
#   # [IDM.OCM.E1.000010]
#      Given endpoint is provided
#        And DID is of OCM
#      When connection is attempted with <DID> using TSA
#      Then the connection is not accepted
#
#    Scenario: A list of all existing connections is provided by an endpoint
#    # [IDM.OCM.E1.00011]
#      When request about all existing connections is attempted
#      Then the response contains existing connections
#
