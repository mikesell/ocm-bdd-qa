Feature: OCM.E1 ConnectionManager
  
  **CM**
    Credential Manager.
  **BCE**
    Block Connection Endpoint.
  **SSIAS**
    SSI Abstraction Service.

    Background: 
    Given Instance Alpha is running
    And Instance Beta is running
    And REST Interfaces are running
    And NATS Interfaces are running
    And CM is running
    And SSIAS is running
    And BCE is provided
        
  @idm-ocm-e1-00006
  Scenario: A User should be able to establish a connection, if a invitation is succesfull
  #Scenario Number 2 in OCM_PCM_Test_Ideas_New_Version.xlsx
    Given ID and DID are not blocked
    When request from instance Alpha is attempted 
    Then  sucessful connection response is sent from instance Beta to instance Alpha
      

  @idm-ocm-e1-00007
  Scenario: A User tries to establish a connection, on a blocked connection
  #Scenario Number 1 in OCM_PCM_Test_Ideas_New_Version.xlsx
    Given ID and DID are blocked
    When request from instance Alpha is attempted 
    Then unsucessful connection response containing error status "404" is sent from instance Beta to instance Alpha
      And connection from SSIAS is deleted
      And connection is marked as blocked in CM
    # testideas which are not tested (Not valid, UNIT test, etc.)
    # info from Michael: both IDs are necessary
    #Verify, that the endpoint accept a the connection ID, without a DID
    #Verify, that the endpoint accept a DID, without a connection ID


  @idm-ocm-e1-00008 @idm-ocm-e1-00009
  Scenario: A User tries to establish a connection, with the DID of the OCM
  #Scenario Number 4 in OCM_PCM_Test_Ideas_New_Version.xlsx
    Given ID and DID are blocked
    When request from instance Alpha is attempted 
    Then sucessful connection response is sent from instance Alpha to instance Alpha automatically
      And connection in SSIAS existss
      And connection is unblocked in CM
      And connection is trusted in CM


  @idm-ocm-e1-00008 @idm-ocm-e1-00009
  Scenario: A User tries to establish a connection, without the DID of the OCM
  #Scenario Number 5 in OCM_PCM_Test_Ideas_New_Version.xlsx
    Given ID and DID are empty
    When request from instance Alpha is attempted
    Then sucessful connection response is not sent automatically from instance Beta to instance Alpha
      And connection in SSIAS existss
      And connection is unblocked in CM
      #research how exactly are the status named, where exactly to check. --> ask the developers

    #[IDM.OCM.E1.00010] TSA Acception
    #The Connection Manager MUST use TSA to accept/block connections automatically according 
    #to the incoming DID.
    #After talk with Steffen: Not required to do a bdd test	It´s more an internal testing thing, so be done in the Unit Tests/ Whitebox testing

    # [IDM.OCM.E1.00011] Connection List Endpoint
    # The Connection Manager MUST provide an Endpoint to List all existing connections. 
    # After talk with Steffen: Not relevant for BDD -> Unit test

